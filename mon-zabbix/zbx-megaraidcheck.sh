#!/bin/sh

MEGACLI1=/opt/MegaRAID/MegaCli/MegaCli
MEGACLI2=/opt/MegaRAID/MegaCli/MegaCli64
MEGACLI3=/usr/sbin/megacli
MEGACLI=""


for i in $(seq 1 3); do
        if test -x "${MEGACLI}${i}"; then
                MEGACLI="${MEGACLI}${i}"
        fi
done

if test

INFO=$($MEGACLI -AdpAllInfo -aAll -NoLog | grep -A 6 -i "Virtual Drives")

VDEGRADED=$(echo "$INFO" |awk -F ':' '{IGNORECASE=1} $1 ~ /degraded/ {print $2}')
VOFFLINE=$(echo "$INFO" |awk -F ':' '{IGNORECASE=1} $1 ~ /offline/ {print $2}')
PCRITICAL=$(echo "$INFO" |awk -F ':' '{IGNORECASE=1} $1 ~ /critical disks/ {print $2}')
POFFLINE=$(echo "$INFO" |awk -F ':' '{IGNORECASE=1} $1 ~ /failed disks/ {print $2}')


#echo $VDEGRADED ", " $VOFFLINE ", " $PCRITICAL "," $POFFLINE
if test -z "$VDEGRADED" || test -z "$VOFFLINE" || test -z "$PCRITICAL" || test -z "$POFFLINE"; then
        echo 101
        exit 0
fi

if test $VDEGRADED -gt 0 || test $VOFFLINE -gt 0 || test $PCRITICAL -gt 0 || test $POFFLINE -gt 0; then
        expr $VDEGRADED + $VOFFLINE + $PCRITICAL + $POFFLINE
else   
        echo 0
fi

exit 0

