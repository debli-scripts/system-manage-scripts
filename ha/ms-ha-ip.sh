#!/bin/sh

SLAVE_IP=192.168.1.191
MASTER_IP=192.168.1.192
FLOAT_IP=192.168.1.193
DRBD_RES=nfs
NFS_DIR=php
MOUNT_POINT=/srv
LOG_FILE=/var/log/shell_ha.log
NOW=`date '+%F %T'`
INTERFACE="eth0"
INTERVAL=5

export PATH="$PATH:/sbin/:/usr/sbin/:/usr/bin"

slave_loop() {
  while true; do
    ping -c1 ${MASTER_IP} >/dev/null 2>&1
    if ! [ $? -eq 0 ];then
      ping -c1 ${FLOAT_IP} >/dev/null 2>&1
      if test $? -eq 0; then
        continue
      fi
      
      echo "${NOW} - Master: ${MASTER_IP} seem down, starting slave..." >> ${LOG_FILE}
      /sbin/drbdadm -- --overwrite-data-of-peer primary ${DRBD_RES} >> ${LOG_FILE} 2>&1
      /etc/init.d/lvm2 start >> ${LOG_FILE} 2>&1
      echo "Mounting nfs res part..." >> ${LOG_FILE}
      mount /dev/${DRBD_RES}/${NFS_DIR} $MOUNT_POINT/${DRBD_RES}/${NFS_DIR}
      if test $? -ne 0; then
        echo "Failed!" >> ${LOG_FILE}
        echo "Terminate!" >> ${LOG_FILE}
        exit 2
      fi
      echo "Done." >> ${LOG_FILE}
      echo "Configuring float ip: ${FLOAT_IP}..." >> ${LOG_FILE}
      /sbin/ifconfig ${INTERFACE} add  $FLOAT_IP netmask 255.255.255.0 up
      arping -A -c 1 -I ${INTERFACE} $FLOAT_IP
      echo "Done." >> ${LOG_FILE}
      /etc/init.d/nfs-kernel-server restart >> ${LOG_FILE} 2>&1
      echo "All Done. exit!" >> ${LOG_FILE}
      kill -9 $$
    fi
    sleep ${INTERVAL} 
  done
}

slave_loop

exit 0
