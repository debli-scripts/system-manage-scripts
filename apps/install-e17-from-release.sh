#!/bin/sh

PKGS="enlightenment"
#PKGS="eina eet evas evas_generic_loaders ecore eio embryo edje efreet e_dbus eeze emotion ethumb elementary"
BASE_DIR="http://download.enlightenment.org/releases/"
ARCH_SUF=".tar.gz"
FORCE=0
INSTALL=0
UNINSTALL=0
KEEP_ARCH=0
#VERSION="1.7.4"
VERSION="0.17.0"
DOWN_DIR="./e17-release/"
USAGE="./script_name -fpv\n"
INSTALL_DIR="/opt/e17/"

while getopts irf:p: f
do
  echo $f
  case $f in
    r)
      UNINSTALL=1
      ;;
    i)
      INSTALL=1
      ;;
    f)
      FORCE=1
      ;;
    p)
      PKG=$(echo "$OPTARG" |cut -d':' -f 2)
      FORCE=1
      ;;
    v)
      VERSION=$f
      ;;
    \?) echo $USAGE; exit 1
      ;;
  esac
done
shift `expr $OPTIND - 1`


download_pkgs() {
  if test -z "$PKGS"; then
    echo "Pkg names is none!"
    exit 1
  fi

  OLD_PWD=`pwd`
  mkdir -p $DOWN_DIR
  cd $DOWN_DIR
  if test $? -ne 0; then
    echo "Unable create dir ./e17-release!"
    exit 2
  fi

  for pkg in $PKGS; do
    #wget -c "${BASE_DIR}${pkg}-${VERSION}${ARCH_SUF}" -O - |tar xvzf -
    wget -c "${BASE_DIR}${pkg}-${VERSION}${ARCH_SUF}"
    if test -d "${pkg}-${VERSION}"; then
      rm -rf "${pkg}-${VERSION}"
    fi

    tar xvzf ${pkg}-${VERSION}${ARCH_SUF}

  done

  cd $OLD_PWD
}

main() {
  if test $INSTALL -ne 1; then
    download_pkgs
    exit 0
  fi
  if test ! -d $DOWN_DIR; then
    echo "Unable found dir : $DOWN_DIR"
    exit 2
  fi

  OLD_DIR=`pwd`

  cd $DOWN_DIR
  if test $? -ne 0; then
    echo "Unable cd dir : $DOWN_DIR"
    exit 3
  fi

  for pkg in $PKGS; do
    cd "./$pkg-$VERSION"
    if test $? -ne 0; then
      echo "Unable cd dir : $DOWN_DIR$pkg-$VERSION"
      exit 3
    fi

    if test $UNINSTALL -eq 1; then
      make uninstall
    fi
    make distclean

    if test -x ./autogen.sh; then
      PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig" ./autogen.sh --prefix=$INSTALL_DIR 
    elif test -x ./configure; then
      PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig" ./configure --prefix=$INSTALL_DIR
    else
      echo "$pkg Missing configure file!"
      exit 1
    fi

    #err_exit "$pkg Configure error!"
    PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig" LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$INSTALL_DIR/lib/" PATH="$PATH:$INSTALL_DIR/bin" make

    #err_exit "$pkg Maker error!"
    PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig" LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$INSTALL_DIR/lib/" PATH="$PATH:$INSTALL_DIR/bin" make install
    cd ../
  done
  cd $OLD_DIR
}

echo $INSTALL
main

# vim: set sw=2 :
