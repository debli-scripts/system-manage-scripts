#!/bin/sh 

INSTALL_USER=ymeng
INSTALL_DIR=/opt/e17/
#SRC_LIST="eina eet PROTO/eobj evas ecore eio embryo edje efreet e_dbus eeze elementary e"
SRC_LIST="edbus efreet e_dbus eeze elementary e"
#SRC_LIST="E-MODULES-EXTRA/comp-scale E-MODULES-EXTRA/cpu E-MODULES-EXTRA/net"
#SRC_LIST="econnman"
BASE_DIR="http://svn.enlightenment.org/svn/e/trunk/" 
FORCEINSTALL=0
LOG_FILE="./installe17.log"
PKG=""

case $1 in
        forceinstall)
                FORCEINSTALL=1
                ;;

        pkg*)
                PKG=$(echo "$1" |cut -d':' -f 2)
                FORCEINSTALL=1
                ;;
esac

log()
{
        if test $1 -eq 1; then
                echo $*
        fi

        echo $3 >> $LOG_FILE

        if test $2 -eq 1; then
                exit 1
        fi
}

err_exit()
{
        if test $? -ne 0; then
                echo $*
                exit 1
        fi
}

sudo mkdir $INSTALL_DIR
sudo chown $INSTALL_USER.$INSTALL_USER $INSTALL_DIR -R

if test ! -z "$PKG"; then
        SRC_LIST=$PKG
fi

for prj in ${SRC_LIST}; do
        oprj=$prj
        prj=$(echo $prj |sed 's/PROTO\///g'|sed 's/E-MODULES-EXTRA\///g')
        echo "Installing $prj..."
        if test -d "./${prj}"; then
                echo "cd ${prj}"
                cd ${prj}
                rm -rf ./po
                SVN_UINFO=$(svn update|grep 'At revision')
                echo $SVN_UINFO
                if test $FORCEINSTALL -eq 0 && test ! -z "$SVN_UINFO"; then
                        cd ../
                        echo "$prj is up to date, ignoring."
                        continue
                fi

                err_exit "$prj SVN update error!"
        else
                svn co $BASE_DIR$oprj
                err_exit "$prj SVN co error!"
                cd ${prj}
        fi

        make distclean
        if test -x ./autogen.sh; then
                PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig" ./autogen.sh --prefix=$INSTALL_DIR 
        elif test -x ./configure; then
                PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig" ./autogen.sh --prefix=$INSTALL_DIR
        else
                echo "$prj Missing configure file!"
                exit 1
        fi

        err_exit "$prj Configure error!"
        PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig" LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$INSTALL_DIR/lib/" PATH="$PATH:$INSTALL_DIR/bin" make

        err_exit "$prj Maker error!"
        PKG_CONFIG_PATH="$PKG_CONFIG_PATH:$INSTALL_DIR/lib/pkgconfig" LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$INSTALL_DIR/lib/" PATH="$PATH:$INSTALL_DIR/bin" make install
        cd ../
done
