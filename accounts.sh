#/bin/sh

################################################################################
# Settings
################################################################################
# remote log server
rlogserver="218.61.76.163"
rlogfile="~/myspace/docs/server_accounts.log"
rlogidrsa="self.id_rsa"
rloguser="ymeng"

# apps
bin_adduser=/usr/sbin/adduser
bin_deluser=/usr/sbin/deluser
bin_passwd=/usr/bin/passwd

# constants
YES_ALL=0

# Log file

LOG_FILE="~/accounts_"
LOG_EXT=".log"


# Ftp settings
DB_LOAD=/usr/bin/db4.5_load
DB_LOAD_OPT=" -T -t hash "

VSFTPD_USER_FILE=/etc/vsftpd/virtual_users
VSFTPD_USER_DB="${USER_FILE}.db"
VSFTPD_USER_CONF_DIR=/etc/vsftpd/vsftpd_user_conf/
VSFTPD_USER_CONF_TPL="
local_root=/data/www
write_enable=YES
anon_world_readable_only=YES
anon_upload_enable=YES
anon_mkdir_write_enable=YES
anon_other_write_enable=YES"

# Password policy

# char dict
RANDOM_SRC_NORMAL="abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXY"
RANDOM_SRC_SPECIAL="*#!()&^,<>\$-+=[]|"
RANDOM_NORMAL_MAX=61
RANDOM_SPECIAL_MAX=18
PASSWD_LENGTH=12

TYPE="ftp"
USER=""
OPE=""
DELETE_HOME=0

################################################################################
# Procedures
################################################################################

stats()
{
    if [ $? -ne 0 ]; then
        msg_quit $1
    fi
}

msg_quit()
{
    echo $*
    exit 10
}

usage()
{
    echo "Usage: ./accounts.sh -u <User name> -t <ftp|db|system> " \
        "-l <stdout|filename|default> -o <create|delete|lock|unlock> -a (delete user home dir)"
    exit 3
}

rlog()
{
    echo $* | ssh -i ${rlogidrsa} ${rloguser}@${rlogserver} "cat >> ${rlogfile}"
}

while getopts "at:l:u:o:yd:" opt; do
    case $opt in
        a) DELETE_HOME=1
            ;;

        t) TYPE=$OPTARG
            ;;

        l) LOG_FILE=$OPTARG
            ;;

        d) DOMAIN=$OPTARG
            ;;

        u) USER=$OPTARG
            ;;

        o) OPE=$OPTARG
            ;;

        y)
            YES_ALL=1
            ;;

        *) usage
            ;;
    esac
done

go_continue ()
{
    input=""

    i=1
    _y="y"
    _n="n"

    ([ "$1" == "y" ] || [ "$1" == "" ]) &&  _y="Y"
    [ "$1" == "n" ] && _n="N"

    if [ $YES_ALL -eq 1 ]; then
        return
    fi
    while [ $i -eq 1 ]; do
        echo -n "Continue? [${_y}/${_n}] "
        read input
        case "$input" in
            "y")
                break
                ;;
            "n")
                exit 0
                ;;

            "")
                if [ "$_y" == "Y" ]; then
                    break;
                elif [ "$_n" == "N" ]; then
                    exit 0
                fi
                ;;
            *)
                continue
                ;;
        esac
    done
}

clean_args()
{
    [ "$TYPE" != "" ] || usage
    [ "$USER" != "" ] || usage
    [ "$OPE" != "" ]  || OPE="create"
    [ "$DELETE_HOME" != "" ] || DELETE_HOME=0
    [ "$LOG_FILE" != "" ] || LOG_FILE="./accounts_${TYPE}.log"
}

gen_random_passwd() 
{
    _PASSWD=""
    for i in $(seq 1 $PASSWD_LENGTH); do
        _normal_index=$(expr $RANDOM % $RANDOM_NORMAL_MAX)
        _special_index=$(expr $RANDOM % $RANDOM_SPECIAL_MAX + 10)
        _PASSWD=${_PASSWD}$(expr substr "$RANDOM_SRC_NORMAL" $_normal_index 1)
        _t=$(expr $RANDOM % 3)
        if [ $_t -eq 0 ]; then
            _PASSWD=${_PASSWD}$(expr substr "$RANDOM_SRC_SPECIAL" $_special_index 1)
        fi
    done
    echo $_PASSWD
#    echo $( cat /dev/urandom |strings -${PASSWD_LENGTH} |head -1 | tr -d '`{}?" ' )
}

check_perm()
{
    [ $UID -eq 0 ] || {
    echo "You must be root!"
    exit 1
}
}

env_check()
{
    [ -x "$DB_LOAD" ] || {
    echo "$DB_LOAD not exists!"
    exit 2
}
}

find_user()
{
    [ "$1" == "" ] && return
    $(cat /etc/passwd |grep $1 > /dev/null 2>&1)
    if [ $? -ne 0 ]; then
        echo 1
    else
        echo 0
    fi
}

get_tip()
{
    ope_msg=""
    case $OPE in
        create)
            ope_msg="Creating"
            ;;
        delete)
            ope_msg="Deleting"
            ;;
        lock)
            ope_msg="Locking"
            ;;
        unlock)
            ope_msg="Unlocking"
            ;;

        *)
            ope_msg="Unsupported operation: ${OPE}"
            echo $type_msg
            exit 4
            ;;
    esac

    echo "${ope_msg} ${TYPE} account: ${USER}..."
}

handle_ftp_account()
{
    PASSWD=$(gen_random_passwd)
    echo $PASSWD
    echo "$DB_LOAD $DB_LOAD_OPT -f $USER_FILE $USER_DB"
}

read_rsa_pub_key()
{
    read key
    echo $key
}

get_ip()
{
    echo "$(/sbin/ifconfig eth0 |grep 'inet addr' |awk '{print $2}' |tr -d [:alpha:]:)"
}

get_issue()
{
    _issue=$(cat /etc/issue |head -1)
    _missue=$(echo ${_issue} | tr '[A-Z]' '[a-z]' |awk '{print $1}')
    echo $_missue
}

debian_adduser_conf_fix()
{
    adduser_conf=/etc/adduser.conf
    missue=$(get_issue)

    echo "Your system is : $missue."
    if [ "$missue" != "debian" ]; then
        return
    fi

    [ ! -f $adduser_conf ] && msg_quit "adduser not installed yet."
    _adduser_mod=$(cat "$adduser_conf"  |grep "DIR_MODE=" | cut -c10-13)

    if [ "${_adduser_mod}" == "" ] || [ ${_adduser_mod} -gt 750 ]; then
        echo "adduser.conf: dir_mode too open : ${_adduser_mod}, fixe it?"
        go_continue
        sed -ig 's/DIR_MODE=.*/DIR_MODE=0750/' "${adduser_conf}"
        stats "Could not modify file: ${adduser_conf}"
        echo 'Done.'
    fi
}

added_hook()
{
    missue=$(get_issue)
    if [ "$missue" != "centos" ]; then
        echo "Change password for $USER..."
        passwd $USER
        stats "Could not change password for $USER"
        echo "Done."
    fi
}

handle_system_account()
{
    home_dir="/home/$USER/"
    case $OPE in
        create)
            debian_adduser_conf_fix
            PASSWD=$(gen_random_passwd)
            tip=$(get_tip)
            echo $tip
            echo "Random user passwd is $PASSWD"
            RSA_PASSWD=$(gen_random_passwd)
            echo "Random rsa passwd is $RSA_PASSWD"
            go_continue

            $bin_adduser $USER
            stats "Could not create user : ${USER} with command $bin_adduser"
            now_time=$(date "+%F %T")
            host_ip=$(get_ip)
            rlog_msg="
${now_time} $host_ip   $TYPE    $USER   $OPE   $PASSWD    $RSA_PASSWD"
            rlog "${rlog_msg}"

            redhat_added_hook

            mkdir -p "${home_dir}.ssh" >/dev/null 2>&1
            if [ $? -ne 0 ]; then
                msg_quit 'Could not create '${home_dir}'.ssh'
            fi
            touch "${home_dir}.ssh/authorized_keys"
            chown "${USER}.${USER}" -R "${home_dir}.ssh/"
            echo "RSA pub key: "
            pub_key=$(read_rsa_pub_key)
            [ "$pub_key" == "" ] && msg_quit "No pub key readed."

            stats 'Could not read pub key, user can not logged'

            echo $pub_key >> "${home_dir}.ssh/authorized_keys"
            echo 'Done.'
            ;;

        delete)
            tip=$(get_tip)
            echo $tip
            #finded=$(find_user)
            #[ "$finded" != "0" ] && msg_quit "Could not find this user: $USER"
            go_continue

            $bin_deluser $USER
            stats 'Could not del user '${USER}
            if [ $DELETE_HOME -eq 1 ]; then
                echo "Removing home dir ${home_dir}..."
                go_continue
                rm -rf "${home_dir}" >/dev/null 2>&1
                stats 'Could not remove home dir '${home_dir}
            fi

            stats 'Cannot delete user '${USER}
            echo 'Done.'
            ;;

    esac
}

main_handle()
{
    case $TYPE in
        ftp|all) 
            handle_ftp_account $USER $OPE
            ;;

        db)
            handle_db_account $USER $DB
            ;;

        system)
            handle_system_account $USER
            ;;

        *)
            usage
            ;;
    esac
}


check_perm
clean_args
env_check
main_handle
