# in centos $IFACE is $DEVICE
_route() {
  if test "$IFACE" = "eth0:0"; then
    PATH="$PATH:/usr/bin:/usr/sbin"
    _GATEWAY_IP=".131"
    _ADDR=`ip addr ls |grep "$IFACE$" |awk '{print $2}' |cut -d'/' -f 1`
    _GATEWAY=`echo $_ADDR |cut -d '.' -f1-3`$_GATEWAY_IP
    _DEV=`echo $IFACE |cut -d ':' -f 1`

    /sbin/ip rule add from $_ADDR table tel
    /sbin/ip route add default via $_GATEWAY dev $_DEV src $_ADDR table tel
  fi
}

_route
